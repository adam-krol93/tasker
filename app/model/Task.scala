package model

import java.sql.Timestamp
import java.sql.Date

import com.google.inject.Inject
import play.api.db.slick.DatabaseConfigProvider
import slick.driver.JdbcProfile
import slick.driver.MySQLDriver.api._

import scala.concurrent.{Await, Future}


/**
  * Created by Adam Król, Atos <adam.krol@atos.net>
  */
case class Task(id: Long, key: String, summary: String, description: String, due_date: Date, project_id: Long, state_id: Long, reporter_id: Long, assignee_id: Long)


class Tasks(tag: Tag) extends Table[Task](tag, "task") {

  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)

  def key = column[String]("task_key")

  def description = column[String]("description")

  def summary = column[String]("summary")

  def reporterId = column[Long]("reporter_id")

  def assigneeId = column[Long]("assignee_id")

  def projectId = column[Long]("project_id")

  def stateId = column[Long]("state_id")

  def dueDate = column[Date]("due_date")

  def * = (id, key, summary, description, dueDate, projectId, stateId, reporterId, assigneeId) <>(Task.tupled, Task.unapply)

  def reporter = foreignKey("reporter", reporterId, TableQuery[UsersTableDef])(_.id)

  def assignee = foreignKey("assignee", assigneeId, TableQuery[UsersTableDef])(_.id)

  def project = foreignKey("project", projectId, TableQuery[Projects])(_.id)

  def state = foreignKey("state", stateId, TableQuery[FlowSteps])(_.id)
}

case class TaskFormData(summary: String, description: String, due_date: Date, project_id: Long, assignee_id: Long)

case class ReassignTaskFormData(assigneeId: Long)

case class ChangeStateTaskFormData(flowId: Long)

class TaskRepository @Inject()(dbConfigProvider: DatabaseConfigProvider) {

  implicit class TaskExtension[C[_]](q: Query[Tasks, Task, C]) {
    def withAssignee = q.join(TableQuery[UsersTableDef]).on(_.assigneeId === _.id)

    def withReporter = q.join(TableQuery[UsersTableDef]).on(_.reporterId === _.id)

    def withProject = q.join(TableQuery[Projects]).on(_.projectId === _.id)

    def withState = q.join(TableQuery[FlowSteps]).on(_.stateId === _.id)

    def withAll = q.join(TableQuery[UsersTableDef]).on(_.assigneeId === _.id)
      .join(TableQuery[UsersTableDef]).on(_._1.reporterId === _.id)
      .join(TableQuery[Projects]).on(_._1._1.projectId === _.id)
      .join(TableQuery[FlowSteps]).on(_._1._1._1.stateId === _.id)
  }

  val dbConfig = dbConfigProvider.get[JdbcProfile]

  val tasks = TableQuery[Tasks]

  def save(task: Task) = task.id match {
    case 0L => dbConfig.db.run(this.tasks += task)
    case _ => dbConfig.db.run(this.tasks.filter(_.id === task.id).update(task))
  }

  def assignedToMe(id: Long): Future[Seq[((((Task, User), User), Project), FlowStep)]] = dbConfig.db.run(this.tasks.withAll.filter(_._1._1._1._1.assigneeId===id).filter(t => t._1._1._1._1.stateId =!= t._1._2.lastStepId).result)

  def reportedByMe(id: Long): Future[Seq[((((Task, User), User), Project), FlowStep)]] = dbConfig.db.run(this.tasks.withAll.filter(_._1._1._1._1.reporterId===id).filter(t => t._1._1._1._1.stateId =!= t._1._2.lastStepId).result)

  def findById(id: Long): Future[Option[((((Task, User), User), Project), FlowStep)]] = dbConfig.db.run(this.tasks.withAll.filter(_._1._1._1._1.id===id).result.headOption)

  def currentCountForProject(id: Long) = dbConfig.db.run(this.tasks.filter(_.projectId === id).length.result)

  def findByKey(key: String) :Future[Option[((((Task, User), User), Project), FlowStep)]] = dbConfig.db.run(this.tasks.withAll.filter(_._1._1._1._1.key===key).result.headOption)

  def findByKeyWithAssigneeOnly(key: String):Future[Option[(Task,User)]] = dbConfig.db.run(this.tasks.withAssignee.filter(_._1.key===key).result.headOption)

  def findByKeyWithFlowStepOnly(key: String):Future[Option[(Task,FlowStep)]] = dbConfig.db.run(this.tasks.withState.filter(_._1.key===key).result.headOption)

  def updateAssignee(id: Long, assignee: Long) = dbConfig.db.run(tasks.filter(_.id===id).map(t => t.assigneeId).update(assignee))

  def updateFlowStep(id: Long, flowStep: Long) = dbConfig.db.run(tasks.filter(_.id===id).map(t => t.stateId).update(flowStep))

}