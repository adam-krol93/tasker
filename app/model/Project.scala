package model

import com.google.inject.Inject
import play.api.db.slick.DatabaseConfigProvider
import slick.driver.JdbcProfile
import slick.driver.MySQLDriver.api._

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}


/**
  * Created by Adam Król, Atos <adam.krol@atos.net>
  */
case class Project(id: Long, key: String, name: String, description: String, managerId: Long, firstStep: Long, lastStep: Long, isDeleted: Boolean)

class Projects(tag: Tag) extends Table[Project](tag, "project") {
  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)

  def key = column[String]("project_key")

  def description = column[String]("description")

  def name = column[String]("name")

  def isDeleted = column[Boolean]("isDeleted")

  def managerId = column[Long]("manager_id")

  def firstStepId = column[Long]("first_step_id")

  def lastStepId = column[Long]("last_step_id")

  def * = (id, key, name, description, managerId, firstStepId, lastStepId, isDeleted) <>(Project.tupled, Project.unapply)

  def manager = foreignKey("manager", managerId, TableQuery[UsersTableDef])(_.id)

  def firstStep = foreignKey("first_step", firstStepId, TableQuery[FlowSteps])(_.id)

  def lastStep = foreignKey("last_step", lastStepId, TableQuery[FlowSteps])(_.id)
}


case class ProjectFormData(key: String, name: String, description: String, managerId: Long, firstStepId: Long, lastStepId: Long)

case class EditProjectFormData(name: String, description: String, managerId: Long, firstStepId: Long, lastStepId: Long)


class ProjectRepository @Inject()(dbConfigProvider: DatabaseConfigProvider) {

  implicit class ProjectExtension[C[_]](q: Query[Projects, Project, C]) {
    def withManager = q.join(TableQuery[UsersTableDef]).on(_.managerId === _.id)
  }

  val dbConfig = dbConfigProvider.get[JdbcProfile]

  val projects = TableQuery[Projects]

  def findProjectByKey(key: String): Future[Option[(Project, User)]] = dbConfig.db.run(projects.filter(x => x.key === key && x.isDeleted === false).withManager.result.headOption)

  def save(project: Project) = project.id match {
    case 0L => dbConfig.db.run(this.projects += project)
    case _ => dbConfig.db.run(this.projects.filter(_.id === project.id).update(project))
  }

  def editProject(id: Long, name: String, description: String, managerId: Long, firstStepId: Long, lastStepId: Long) = {
    dbConfig.db.run(projects.filter(_.id === id).map(p => (p.name, p.description, p.managerId,p.firstStepId,p.lastStepId)).update(name, description, managerId,firstStepId,lastStepId))
  }

  def delete(id: Long): Unit = {
    dbConfig.db.run(projects.filter(_.id === id).map(p => p.isDeleted).update(true))
  }

  def exists(key: String): Boolean = Await.result(dbConfig.db.run(projects.filter(x => x.key === key && x.isDeleted === false).exists.result), Duration.Inf)

  def existsWithFirstStep(id: Long): Boolean = Await.result(dbConfig.db.run(projects.filter(x => x.id===id && x.firstStepId == Nil).exists.result),Duration.Inf)

  def findProjectById(id: Long): Future[Option[Project]] = dbConfig.db.run(projects.filter(x => x.id === id && x.isDeleted === false).result.headOption)

  def findAll(): Future[Seq[(Project, User)]] = dbConfig.db.run(projects.filter(_.isDeleted === false).withManager.result)

  def findByUser(id: Long): Future[Seq[Project]] = dbConfig.db.run(projects.filter(_.isDeleted === false).filter(_.managerId === id).result)

}
