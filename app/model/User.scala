package model


import javax.inject.Inject

import be.objectify.deadbolt.scala.models.{Permission, Role, Subject}
import play.api.Logger
import play.api.db.slick.DatabaseConfigProvider
import slick.driver.JdbcProfile
import slick.driver.MySQLDriver.api._


import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

/**
  * Created by Adam Król, Atos <adam.krol@atos.net>
  */
case class User (id: Long, username: String, password: String, rolesv : List[SecurityRole]) extends Subject {
  override def identifier: String = username

  override def roles: List[SecurityRole] = rolesv

  override def permissions: List[Permission] = List(UserPermission("printers.edit"))
}



case class UserFormData (username: String, password: String, role: List[String])


class UsersRepository @Inject() (dbConfigProvider: DatabaseConfigProvider){

  val dbConfig = dbConfigProvider.get[JdbcProfile]

  val users = TableQuery[UsersTableDef]

  def find(id: Long): Option[User] = {
    Await.result(dbConfig.db.run(users.filter(_.id === id).result.headOption),Duration.Inf)
  }

  def findByUserName(username: String): Option[User] = {
    Await.result(dbConfig.db.run(users.filter(_.username === username).result.headOption), Duration.Inf)
  }

  def findByUserNameAndPassword(username: String, password: String): Option[User] = {
    Await.result(dbConfig.db.run(users.filter(_.username === username).filter(_.password === password).result.headOption), Duration.Inf)
  }

  def findAll() : Seq[User] = {
    Await.result(dbConfig.db.run(users.result),Duration.Inf)
  }

  def addNewUser(username: String, password: String, role: List[SecurityRole]) = {
    val user = User(1L,username, password, role)
    Await.result(dbConfig.db.run(this.users+=user), Duration.Inf)
  }

  def deleteUserByID(id: Long)= {
    Await.result(dbConfig.db.run(users.filter(_.id === id).delete), Duration.Inf)
  }

  def editUser(id: Long, username: String, password: String, role: List[SecurityRole])={
    val updatedUser = User(id, username, password, role)
    Await.result(dbConfig.db.run(users.filter(_.id === id).update(updatedUser)),Duration.Inf)
  }
}

class UsersTableDef(tag: Tag) extends Table[User](tag, "users") {
  def id = column[Long]("id",O.PrimaryKey,O.AutoInc)
  def username = column[String]("username")
  def password = column[String]("password")
  implicit val rolesTypeMapper = MappedColumnType.base[List[SecurityRole], String](
    {roleSet => roleSet.map(_.name).mkString(",")},
    {str =>
      if (str.isEmpty)
        List[SecurityRole]()
      else
        str.split(",").map(SecurityRole(_)).toList
    }
  )

  def roles = column[List[SecurityRole]]("roles")

  override def * = (id,username,password,roles) <>(User.tupled, User.unapply)
}
