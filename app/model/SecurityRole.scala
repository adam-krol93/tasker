package model

import javax.inject.Inject

import be.objectify.deadbolt.scala.models.{Permission, Role, Subject}
import play.api.Logger
import play.api.db.slick.DatabaseConfigProvider
import slick.driver.JdbcProfile
import slick.driver.MySQLDriver.api._

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

/**
  * Created by Adam Król, Atos <adam.krol@atos.net>
  */
case class SecurityRole(name :String) extends Role {
  override def toString(): String =
  {
    return this.name
  }
}

object SecurityRole {
  val values = Seq(SecurityRole("admin"),SecurityRole("member"))
}


