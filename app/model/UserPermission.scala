package model

import be.objectify.deadbolt.scala.models.Permission

/**
  * Created by Adam Król, Atos <adam.krol@atos.net>
  */
case class UserPermission(value: String) extends Permission
