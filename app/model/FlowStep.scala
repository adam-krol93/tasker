package model

import slick.lifted.Tag
import com.google.inject.Inject
import play.api.db.slick.DatabaseConfigProvider
import slick.driver.JdbcProfile
import slick.driver.MySQLDriver.api._
import scala.concurrent.ExecutionContext.Implicits.global


import scala.concurrent.Future


/**
  * Created by Adam Król, Atos <adam.krol@atos.net>
  */
case class FlowStep(id: Long, name: String)

class FlowSteps(tag: Tag) extends Table[FlowStep](tag,"flow_step") {
  def id = column[Long]("id",O.PrimaryKey,O.AutoInc)
  def name = column[String]("flow_name")
  def * = (id,name)<>(FlowStep.tupled,FlowStep.unapply)
}


case class FlowStepToFlowStep(flowId: Long, outgoingFlowId:Long)

case class FlowStepFormData(name: String)

case class OutgoingFlowFormData(outgoingFlows: List[Long])

class FlowStepToFlowSteps(tag: Tag) extends Table[FlowStepToFlowStep](tag,"flow_step_to_flow_step") {
  def flow = column[Long]("flow_id")
  def outgoingFlow = column[Long]("outgoing_flow_id")
  def * = (flow,outgoingFlow)<>(FlowStepToFlowStep.tupled,FlowStepToFlowStep.unapply)

  def pk = primaryKey("flow_to_flow_pk",(flow,outgoingFlow))

  def flowFK = foreignKey("flow_FK",flow,TableQuery[FlowSteps])(_.id,onDelete = ForeignKeyAction.Cascade)
  def outgoingFlowFK = foreignKey("outgoing_flow_FK",outgoingFlow,TableQuery[FlowSteps])(_.id,onDelete = ForeignKeyAction.Cascade)

}

class FlowStepRepository @Inject()(dbConfigProvider: DatabaseConfigProvider) {

  implicit class FlowStepExtension[C[_]](q:Query[FlowSteps,FlowStep,C]) {
    def withOutgoingSteps = q.join(TableQuery[FlowStepToFlowSteps]).on(_.id===_.flow)
      .join(TableQuery[FlowSteps]).on(_._2.outgoingFlow===_.id)
  }

  val dbConfig = dbConfigProvider.get[JdbcProfile]

  val flowSteps = TableQuery[FlowSteps]

  val flowStepToFlowSteps = TableQuery[FlowStepToFlowSteps]

  def findById(id :Long) : Future[Option[FlowStep]] = dbConfig.db.run(flowSteps.filter(_.id===id).result.headOption)

  def findAll() : Future[Seq[FlowStep]] = dbConfig.db.run(flowSteps.result)

  def findAllWithout(id: Long) = dbConfig.db.run(flowSteps.filter(_.id =!= id).result)

  def findAllWithOutgoingSteps() : Future[Seq[((FlowStep, FlowStepToFlowStep), FlowStep)]] = dbConfig.db.run(flowSteps.withOutgoingSteps.result)

  def findByIdWithOutgoingSteps(id : Long) : Future[Option[((FlowStep, FlowStepToFlowStep), FlowStep)]] = dbConfig.db.run(flowSteps.withOutgoingSteps.filter(_._2.id===id).result.headOption)

  def save(flow: FlowStep) = flow.id match {
    case 0L => dbConfig.db.run(this.flowSteps+=flow)
    case _ => dbConfig.db.run(flowSteps.filter(f => f.id === flow.id).update(flow))
  }

  def findOutgoingFlowsId(id: Long) : Future[Seq[Long]] = dbConfig.db.run(this.flowStepToFlowSteps.filter(_.flow===id).map(_.outgoingFlow).result)

  def save(id:Long, outgoingFlows: List[FlowStepToFlowStep]): Unit = {
    dbConfig.db.run((for {
      q1 <- this.flowStepToFlowSteps.filter(_.flow===id).delete
      q2 <- this.flowStepToFlowSteps++=outgoingFlows
    } yield(q1,q2)).transactionally)
  }

}
