package modules

import javax.inject.Inject

import be.objectify.deadbolt.scala.cache.HandlerCache
import be.objectify.deadbolt.scala.{DeadboltHandler, HandlerKey}

/**
  * Created by Adam Król, Atos <adam.krol@atos.net>
  */
class DeadboltCache @Inject() (deadboltHandler: DeadboltHandler) extends HandlerCache {

  override def apply(v1: HandlerKey): DeadboltHandler = deadboltHandler

  override def apply(): DeadboltHandler = deadboltHandler

}
