package modules

/**
  * Created by Adam Król, Atos <adam.krol@atos.net>
  */

import be.objectify.deadbolt.scala.DeadboltHandler
import be.objectify.deadbolt.scala.cache.HandlerCache
import security.AuthSupport
import play.api.inject.{Binding, Module}
import play.api.{Configuration, Environment}

class CustomBindings extends Module  {
  override def bindings(environment: Environment,
                        configuration: Configuration): Seq[Binding[_]] =
    Seq(
      bind[DeadboltHandler].to[MyDeadboltHandler],
      bind[AuthSupport].toSelf,
      bind[HandlerCache].to[DeadboltCache]
      // other bindings, such as HandlerCache
    )
}