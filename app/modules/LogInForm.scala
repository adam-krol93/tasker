package modules

import play.api.data.Form
import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._


/**
  * Created by Adam Król, Atos <adam.krol@atos.net>
  */
object LogInForm {
  def logInForm: Form[(String, String)] = Form(
    tuple(
      "email" -> text,
      "password" -> text)
  )

}
