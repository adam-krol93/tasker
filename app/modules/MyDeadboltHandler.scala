package modules
import javax.inject.{Inject, Singleton}

import be.objectify.deadbolt.scala.models.Subject
import be.objectify.deadbolt.scala.{AuthenticatedRequest, DeadboltHandler, DynamicResourceHandler}
import model.User
//import models.{LogInForm, User}
import play.api.mvc.{Request, Result, Results}
import play.twirl.api.HtmlFormat
import security.AuthSupport

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

@Singleton
class MyDeadboltHandler @Inject() (authSupport: AuthSupport) extends DeadboltHandler {

  override def beforeAuthCheck[A](request: Request[A]): Future[Option[Result]] = Future {None}

  override def getDynamicResourceHandler[A](request: Request[A]): Future[Option[DynamicResourceHandler]] = Future {None}

  /**
    * Get the current user.
    *
    * @param request the HTTP request
    * @return a future for an option maybe containing the subject
    */
  override def getSubject[A](request: AuthenticatedRequest[A]): Future[Option[Subject]] =
    Future {
      request.subject.orElse {
        request.session.get("username") match {
          case Some(userId) => authSupport.getUser(userId)
          case _ => None
        }
      }}

  /**
    * Handle instances of authorization failure.
    *
    * @param request the HTTP request
    * @return either a 401 or 403 response, depending on the situation
    */
  override def onAuthFailure[A](request: AuthenticatedRequest[A]): Future[Result] = Future{
    Results.Redirect("/login")
    //    def toContent(maybeSubject: Option[Subject]): (Boolean, HtmlFormat.Appendable) =
//      maybeSubject.map(subject => subject.asInstanceOf[User])
//        .map(user => (true, views.html.index))
//        .getOrElse {(false, views.html.loginForm(LogInForm.logInForm))}
//
//
//    //        .map(user => (true, denied(Some(user))))
//    //        .getOrElse {(false, views.html.security.logIn(LogInForm.logInForm))}
//
//    getSubject(request).map(maybeSubject => toContent(maybeSubject))
//      .map(subjectPresentAndContent =>
//        if (subjectPresentAndContent._1) Results.Forbidden(subjectPresentAndContent._2)
//        else Results.Unauthorized(subjectPresentAndContent._2))
  }
}