package security

import javax.inject.{Inject, Singleton}

import model.{User, UsersRepository}
import play.api.db.slick.DatabaseConfigProvider

/**
  * Created by Adam Król, Atos <adam.krol@atos.net>
  */
@Singleton
class AuthSupport @Inject()(dbConfigProvider: DatabaseConfigProvider, usersRepository: UsersRepository) {
  def getUser(userId: String): Option[User] = usersRepository.findByUserName(userId)
}
