package controllers

import javax.inject.{Inject, Singleton}

import be.objectify.deadbolt.scala.{ActionBuilders, DeadboltActions}
import model._
import modules.MyDeadboltHandler
import play.api.data._
import play.api.data.Forms._

import play.api.mvc.{Action, Controller}
import security.AuthSupport

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

import com.github.t3hnar.bcrypt._

/**
  * Created by Adam Król, Atos <adam.krol@atos.net>
  */
@Singleton
class UsersController @Inject() (actionBuilder : ActionBuilders, deadboltActions: DeadboltActions, authSupport: AuthSupport, deadboltHandler: MyDeadboltHandler, usersRepository: UsersRepository) extends Controller{

  var formUser = Form(
    mapping(
      "username" -> nonEmptyText,
      "password" -> nonEmptyText,
      "role" -> list(nonEmptyText)
    )(UserFormData.apply)(UserFormData.unapply)
  )

  def listUsers = actionBuilder.RestrictAction("admin").defaultHandler() { authRequest => Future{
      val users = usersRepository.findAll()
      Ok(views.html.users.index(users)(deadboltHandler)(authRequest))
    }
  }


  def addUser() = actionBuilder.RestrictAction("admin").defaultHandler() { authRequest => Future {
    Ok(views.html.users.form(formUser)(authRequest))
    }
  }

  def delete(id: Long) = actionBuilder.RestrictAction("admin").defaultHandler() { implicit authRequest => Future {
    usersRepository.deleteUserByID(id)
    Redirect(routes.UsersController.listUsers())
    }
  }

  def edit(id: Long) = actionBuilder.RestrictAction("admin").defaultHandler() { authRequest => Future {
    val foundUser = usersRepository.find(id).get
    formUser = formUser.fill(UserFormData(foundUser.username,"",SecurityRole.values.map(x => if (foundUser.roles.contains(SecurityRole(x.name))) x.name else "").toList))
    Ok(views.html.users.edit(foundUser)(formUser)(authRequest))
    }
  }

  def submitEdit(id: Long) = actionBuilder.RestrictAction("admin").defaultHandler() { implicit authRequest => Future {
    val userFormData = formUser.bindFromRequest().get
    usersRepository.editUser(id, userFormData.username, userFormData.password.bcrypt, userFormData.role
      .map(x => SecurityRole(x)):List[SecurityRole])
    Redirect(routes.UsersController.listUsers())
    }
  }

  def submit = actionBuilder.RestrictAction("admin").defaultHandler() { implicit authRequest => Future {
    val userFormData = formUser.bindFromRequest().get
    usersRepository.addNewUser(userFormData.username, userFormData.password.bcrypt, userFormData.role
      .map(x => SecurityRole(x)):List[SecurityRole])
      Redirect(routes.UsersController.listUsers())
    }
  }
}
