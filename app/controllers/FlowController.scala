package controllers

import javax.inject.{Inject, Singleton}

import be.objectify.deadbolt.scala.{ActionBuilders, DeadboltActions}
import model._
import modules.MyDeadboltHandler
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc.Controller
import security.AuthSupport
import play.api.data._
import play.api.data.Forms._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

/**
  * Created by Adam Król, Atos <adam.krol@atos.net>
  */
@Singleton
class FlowController @Inject()(actionBuilder: ActionBuilders,
                              deadboltActions: DeadboltActions,
                              authSupport: AuthSupport,
                              deadboltHandler: MyDeadboltHandler,
                               flowStepRepository: FlowStepRepository,
                              val messagesApi: MessagesApi) extends Controller with I18nSupport {

  val flowForm = Form(
    mapping(
      "name" -> nonEmptyText
    )(FlowStepFormData.apply)(FlowStepFormData.unapply)
  )

  val outgoingFlows = Form(
    mapping(
      "outgoing_flows" -> list(longNumber)
    )(OutgoingFlowFormData.apply)(OutgoingFlowFormData.unapply)
  )


  def listFlows = actionBuilder.RestrictAction("admin").defaultHandler() { implicit authRequest =>
    flowStepRepository.findAll().map(result => Ok(views.html.flows.index(result)))
  }

  def outGoingFlowsForm(id: Long) = actionBuilder.RestrictAction("admin").defaultHandler() { implicit authRequest =>
    flowStepRepository.findAllWithout(id).map(result => Ok(views.html.flows.out_form(outgoingFlows.fill(
      new OutgoingFlowFormData(Await.result(flowStepRepository.findOutgoingFlowsId(id),Duration.Inf).toList)),
      result,
      Await.result(flowStepRepository.findById(id),Duration.Inf).get)))
  }

  def outGoingFlowsSubmit(id: Long) = actionBuilder.RestrictAction("admin").defaultHandler() { implicit authRequest => Future {
    outgoingFlows.bindFromRequest().fold(
      formWithErrors => {
        BadRequest(views.html.flows.out_form(formWithErrors,Await.result(flowStepRepository.findAllWithout(id),Duration.Inf),Await.result(flowStepRepository.findById(id),Duration.Inf).get))
      },
      flowData => {
        flowStepRepository.save(id,flowData.outgoingFlows.map(l => new FlowStepToFlowStep(id,l)))
        Redirect(routes.FlowController.listFlows())
      }
    )
  }}

  def flowStepFrom = actionBuilder.RestrictAction("admin").defaultHandler() { implicit authRequest => Future {
    Ok(views.html.flows.form(flowForm))
  }}

  def addFlowStepSubmit() = actionBuilder.RestrictAction("admin").defaultHandler(){ implicit authRequest => Future {
    flowForm.bindFromRequest().fold(
      formWithErrors => {
        BadRequest(views.html.flows.form(formWithErrors))
      },
      flowData => {
        val flow = new FlowStep(0L,flowData.name)
        flowStepRepository.save(flow)
        Redirect(routes.FlowController.listFlows())
      }
    )
  }}

  def editFlowStepForm(id:Long) = actionBuilder.RestrictAction("admin").defaultHandler() { implicit authRequest =>
    flowStepRepository.findById(id).map(result => Ok(views.html.flows.edit(flowForm.fill(new FlowStepFormData(result.get.name)),id)))
  }

  def submitEditFlowStepForm(id : Long)  = actionBuilder.RestrictAction("admin").defaultHandler(){ implicit authRequest => Future {
    flowForm.bindFromRequest().fold(
      formWithErrors => {
        BadRequest(views.html.flows.edit(formWithErrors,id))
      },
      flowData => {
        val flow = new FlowStep(id,flowData.name)
        flowStepRepository.save(flow)
        Redirect(routes.FlowController.listFlows())
      }
    )
  }}

}
