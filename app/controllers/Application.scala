package controllers

import javax.inject.Inject

import be.objectify.deadbolt.scala.{ActionBuilders, DeadboltActions, DeadboltHandler}
import model.{TaskRepository, UsersRepository}
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._
import play.api.i18n.{I18nSupport, Messages, MessagesApi}
import services.AuthenticateService
import views._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class Application @Inject()(actionBuilder: ActionBuilders,
                            deadboltActions: DeadboltActions,
                            usersRepository: UsersRepository,
                            messages: MessagesApi,
                            deadboltHandler: DeadboltHandler,
                            authenticateService: AuthenticateService,
                            taskRepository: TaskRepository) extends Controller with I18nSupport {

  def login = deadboltActions.SubjectNotPresent()() { implicit request => Future {
    Ok(html.login(loginForm))
  }
  }

  def authenticate = deadboltActions.SubjectNotPresent()() { implicit request => Future {
    loginForm.bindFromRequest.fold(
      formWithErrors => BadRequest(html.login(formWithErrors)),
      user => Redirect(routes.HomeController.index()).withSession("username" -> user._1)
    )
  }
  }

  def logout = Action {
    Redirect(routes.HomeController.index()).withNewSession.flashing("success" -> "Zostałeś wylogowany")
  }


  def loginForm = Form(tuple(
    "username" -> nonEmptyText,
    "password" -> nonEmptyText
  ) verifying(
    "Niepoprawne dane logowania", result => result match {
    case (username, password) => authenticateService.isUserCorrect(username, password)
  }))

  override def messagesApi: MessagesApi = messages
}