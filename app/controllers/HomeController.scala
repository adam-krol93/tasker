package controllers

import javax.inject._

import be.objectify.deadbolt.scala.{ActionBuilders, DeadboltActions}
import model.{TaskRepository, UsersRepository}
import modules.MyDeadboltHandler
import play.api._
import play.api.mvc._
import security.AuthSupport

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
  * This controller creates an `Action` to handle HTTP requests to the
  * application's home page.
  */
@Singleton
class HomeController @Inject()(actionBuilder: ActionBuilders,usersRepository: UsersRepository, taskRepository: TaskRepository, deadboltActions: DeadboltActions, authSupport: AuthSupport, deadboltHandler: MyDeadboltHandler) extends Controller {

  /**
    * Create an Action to render an HTML page with a welcome message.
    * The configuration in the `routes` file means that this method
    * will be called when the application receives a `GET` request with
    * a path of `/`.
    */
  //  def index = Action {
  //    Ok(views.html.index("Your new application is ready."))
  //  }

  def index = deadboltActions.SubjectPresent()() { authRequest =>
    val currentUser = usersRepository.findByUserName(authRequest.subject.get.identifier).get
    taskRepository.assignedToMe(currentUser.id).map(result =>
      Ok(views.html.index(result)(deadboltHandler)(authRequest))
    )
  }

}
