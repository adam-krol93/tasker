package controllers

import javax.inject.{Inject, Singleton}

import be.objectify.deadbolt.scala.{ActionBuilders, DeadboltActions}
import model._
import modules.MyDeadboltHandler
import play.api.data.Form
import play.api.data.Forms._
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc.Controller
import security.AuthSupport

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}

/**
  * Created by Adam Król, Atos <adam.krol@atos.net>
  */

@Singleton
class TaskController @Inject()(actionBuilder: ActionBuilders,
                               deadboltActions: DeadboltActions,
                               authSupport: AuthSupport,
                               deadboltHandler: MyDeadboltHandler,
                               usersRepository: UsersRepository,
                               projectRepository: ProjectRepository,
                               taskRepository: TaskRepository,
                               flowStepRepository: FlowStepRepository,
                               val messagesApi: MessagesApi) extends Controller with I18nSupport {


  val taskForm = Form(
    mapping(
      "summary" -> nonEmptyText,
      "description" -> nonEmptyText,
      "due_date" -> sqlDate("MM/dd/yyyy"),
      "project_id" -> longNumber.verifying("Projekt nie ma zdefiniowanego pierwszego kroku",!projectRepository.existsWithFirstStep(_)),
      "assignee_id" -> longNumber
    )(TaskFormData.apply)(TaskFormData.unapply)
  )

  val reassignTaskForm = Form(
    mapping("assignee_id"->longNumber)(ReassignTaskFormData.apply)(ReassignTaskFormData.unapply)
  )

  val changeStateTaskForm = Form(
    mapping("step_id"->longNumber)(ChangeStateTaskFormData.apply)(ChangeStateTaskFormData.unapply)
  )

  def showTask(key: String) = actionBuilder.SubjectPresentAction().defaultHandler() { implicit authRequest =>
    taskRepository.findByKey(key).map(result => {
      Ok(views.html.tasks.show(result))
    })
  }

  def reassign(key: String) = actionBuilder.SubjectPresentAction().defaultHandler() { implicit authRequest =>
    val users = usersRepository.findAll()
    taskRepository.findByKeyWithAssigneeOnly(key).map(result => {
      Ok(views.html.tasks.reassign(reassignTaskForm.fill(new ReassignTaskFormData(result.get._2.id)),users,result.get._1.id))
    })
  }

  def reassignSubmit(id: Long) = actionBuilder.SubjectPresentAction().defaultHandler() { implicit authRequest => Future {
    reassignTaskForm.bindFromRequest().fold(
      formWithErrors => {
        BadRequest(views.html.tasks.show(Await.result(taskRepository.findById(id), Duration.Inf)))
      },
      taskData => {
        taskRepository.updateAssignee(id,taskData.assigneeId)
        Redirect(routes.TaskController.showTask(Await.result(taskRepository.findById(id), Duration.Inf).get._1._1._1._1.key))
      }
    )
  }
  }

  def changeState(key:String) = actionBuilder.SubjectPresentAction().defaultHandler() { implicit authRequest =>
    val steps = Await.result(flowStepRepository.findAll(),Duration.Inf)
    taskRepository.findByKeyWithAssigneeOnly(key).map(result => {
      Ok(views.html.tasks.changeState(changeStateTaskForm.fill(new ChangeStateTaskFormData(result.get._1.state_id)),steps,result.get._1.id))
    })
  }

  def submitChangeState(id: Long) = actionBuilder.SubjectPresentAction().defaultHandler() { implicit authRequest => Future {
    changeStateTaskForm.bindFromRequest().fold(
      formWithErrors => {
        BadRequest(views.html.tasks.show(Await.result(taskRepository.findById(id), Duration.Inf)))
      },
      taskData => {
        taskRepository.updateFlowStep(id,taskData.flowId)
        Redirect(routes.TaskController.showTask(Await.result(taskRepository.findById(id), Duration.Inf).get._1._1._1._1.key))
      }
    )
  }
  }

  def assignedToMe() = actionBuilder.SubjectPresentAction().defaultHandler(){ implicit authRequest =>
    val currentUser = usersRepository.findByUserName(authRequest.subject.get.identifier).get
    taskRepository.assignedToMe(currentUser.id).map(result =>
      Ok(views.html.tasks.myTasks("Zadania przypisane do mnie", result))
    )
  }

  def reportedByMe() = actionBuilder.SubjectPresentAction().defaultHandler(){ implicit authRequest =>
    val currentUser = usersRepository.findByUserName(authRequest.subject.get.identifier).get
    taskRepository.reportedByMe(currentUser.id).map(result =>
      Ok(views.html.tasks.myTasks("Zadania utworzone przeze mnie", result))
    )
  }

  def addTaskForm = actionBuilder.RestrictAction("admin").defaultHandler() { implicit authRequest =>
    val users = usersRepository.findAll()
    val currentUser = usersRepository.findByUserName(authRequest.subject.get.identifier)
    projectRepository.findByUser(currentUser.get.id).map(result => {
      Ok(views.html.tasks.form(taskForm, users, result))
    })
  }

  def submitAddForm = actionBuilder.RestrictAction("admin").defaultHandler() { implicit authRequest => Future {
    val currentUser = usersRepository.findByUserName(authRequest.subject.get.identifier).get
    taskForm.bindFromRequest().fold(
      formWithErrors => {
        BadRequest(views.html.tasks.form(formWithErrors, usersRepository.findAll(), Await.result(
          projectRepository.findByUser(currentUser.id), Duration.Inf)))
      },
      taskData => {
        val project = Await.result(projectRepository.findProjectById(taskData.project_id),Duration.Inf).get
        synchronized {
          val actualCount = Await.result(taskRepository.currentCountForProject(taskData.project_id),Duration.Inf)
          val task = new Task(0L, project.key+"-"+(actualCount+1), taskData.summary, taskData.description, taskData.due_date, taskData.project_id, project.firstStep, currentUser.id, taskData.assignee_id)
          taskRepository.save(task)
        }
        Redirect(routes.TaskController.reportedByMe())
      }
    )
  }
  }


}
