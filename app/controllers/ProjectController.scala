package controllers

import javax.inject.{Inject, Singleton}

import be.objectify.deadbolt.scala.{ActionBuilders, DeadboltActions}
import model._
import modules.MyDeadboltHandler
import play.api.data._
import play.api.data.Forms._
import play.api.i18n.{I18nSupport, MessagesApi}
import security.AuthSupport

import scala.concurrent.ExecutionContext.Implicits.global
import play.api.mvc._

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

/**
  * Created by Adam Król, Atos <adam.krol@atos.net>
  */
@Singleton
class ProjectController @Inject()(actionBuilder: ActionBuilders,
                                  deadboltActions: DeadboltActions,
                                  authSupport: AuthSupport,
                                  deadboltHandler: MyDeadboltHandler,
                                  usersRepository: UsersRepository,
                                  projectRepository: ProjectRepository,
                                  flowStepRepository: FlowStepRepository,
                                  val messagesApi: MessagesApi) extends Controller with I18nSupport{


  var formProject = Form(
    mapping(
      "key" -> nonEmptyText(minLength = 3, maxLength = 15)
        .verifying("Taki klucz już istenieje",!projectRepository.exists(_))
        .verifying("Pole może składać się tylko z liter od a do z, bez polskich znaków", _.matches("[a-zA-Z]+")),
      "name" -> nonEmptyText,
      "description" -> nonEmptyText,
      "manager_id" -> longNumber,
      "first_step_id" -> longNumber,
      "last_step_id" -> longNumber
    )(ProjectFormData.apply)(ProjectFormData.unapply)
  )

  val editProjectForm = Form(
    mapping(
      "name" -> nonEmptyText,
      "description" -> nonEmptyText,
      "manager_id" -> longNumber,
      "first_step_id" -> longNumber,
      "last_step_id" -> longNumber
    )(EditProjectFormData.apply)(EditProjectFormData.unapply)
  )


  def listProjects = actionBuilder.SubjectPresentAction().defaultHandler() {implicit  authRequest =>  {
      projectRepository.findAll().map( result => Ok(views.html.projects.index(result)))
    }
  }

  def addProjectForm = actionBuilder.RestrictAction("admin").defaultHandler() { implicit authRequest =>
      val users = usersRepository.findAll()
      flowStepRepository.findAll().map(result => {
        Ok(views.html.projects.form(formProject,users,result))
      })
  }

  def edit(id : Long) = actionBuilder.RestrictAction("admin").defaultHandler() { implicit authRequest =>
    projectRepository.findProjectById(id).map(result => Ok(views.html.projects.edit(usersRepository.findAll(),
      editProjectForm.fill(new EditProjectFormData(result.get.name,result.get.description,result.get.managerId,result.get.firstStep,result.get.lastStep)),id,
      Await.result(flowStepRepository.findAll(),Duration.Inf))))
  }

  def submitEdit(id: Long) = actionBuilder.RestrictAction("admin").defaultHandler() { implicit authRequest => Future {
    editProjectForm.bindFromRequest().fold(
      formWithErrors => {
        BadRequest(views.html.projects.edit(usersRepository.findAll(),formWithErrors,id,Await.result(flowStepRepository.findAll(),Duration.Inf)))
      },
      projectData=> {
        projectRepository.editProject(id,projectData.name,projectData.description,projectData.managerId,projectData.firstStepId,projectData.lastStepId)
        Redirect(routes.ProjectController.listProjects())
      }
    )
  }}

  def delete(id: Long) = actionBuilder.RestrictAction("admin").defaultHandler() { authRequest => Future {
      projectRepository.delete(id)
      Redirect(routes.ProjectController.listProjects())
    }
  }

  def submitProjectForm = actionBuilder.RestrictAction("admin").defaultHandler() { implicit authRequest => Future {
    formProject.bindFromRequest().fold(
      formWithErrors => {
        BadRequest(views.html.projects.form(formWithErrors,usersRepository.findAll(),Await.result(flowStepRepository.findAll(),Duration.Inf)))
      },
      projectData => {
        val project = new Project(0L,projectData.key.toUpperCase,projectData.name,projectData.description,projectData.managerId,projectData.firstStepId, projectData.lastStepId, false)
        projectRepository.save(project)
        Redirect(routes.ProjectController.listProjects())
      }
    )
  }}
}



