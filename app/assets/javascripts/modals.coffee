
($ "button[data-toggle=modal]").on "click", ->
  target = ($ @).attr('data-target')
  url = ($ @).attr('href')
  ($ target).load(url)