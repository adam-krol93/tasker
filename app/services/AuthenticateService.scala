package services

import javax.inject.{Inject, Singleton}

import com.github.t3hnar.bcrypt._
import model.{User, UsersRepository}

/**
  * Created by Adam Król, Atos <adam.krol@atos.net>
  */
@Singleton
class AuthenticateService @Inject() (usersRepository: UsersRepository) {

  def isUserCorrect(username:String,password:String) : Boolean = {
    val user = usersRepository.findByUserName(username).get
    password.isBcrypted(user.password)
  }

}
