import helpers.LoginHelper
import org.scalatest.BeforeAndAfterEach
import org.scalatestplus.play.{HtmlUnitFactory, OneBrowserPerTest, OneServerPerTest, PlaySpec}

/**
  * Created by Adam Król, Atos <adam.krol@atos.net>
  */
class TaskSpec extends PlaySpec with OneServerPerTest with OneBrowserPerTest with HtmlUnitFactory with BeforeAndAfterEach   {



  "Application" should {
    "show list of tasks assigned to me" in {
      LoginHelper.login()
      go to ("http://localhost:" + port + "/tasks/assigned_to_me")
      pageTitle must be("Lista twoich zadań")
      pageSource must include("Zadania przypisane do mnie")
    }

    "show list of tasks reported by me" in {
      LoginHelper.login()
      go to ("http://localhost:" + port + "/tasks/reported_by_me")
      pageTitle must be("Lista twoich zadań")
      pageSource must include("Zadania utworzone przeze mnie")
    }

    "give a possibility to add a task" in {
      LoginHelper.login()
      go to ("http://localhost:" + port + "/tasks/assigned_to_me")
      click on linkText("Dodaj Zadanie")
      textField("summary").value = "Zadanie-1"
      singleSel("assignee_id").value = "1"
      textArea("description").value = "aaaaa"
      singleSel("project_id").value = "1"
      dateTimeField("due_date").value = "07/13/2016"
      submit()
      currentUrl must be ("http://localhost:" + port + "/tasks/reported_by_me")
      pageSource must include ("Zadanie-1")
    }

    "give a possibility to see a task" in {
      LoginHelper.login()
      go to ("http://localhost:" + port + "/tasks/assigned_to_me")
      click on linkText("Dodaj Zadanie")
      textField("summary").value = "Zadanie-1"
      singleSel("assignee_id").value = "1"
      textArea("description").value = "aaaaa"
      singleSel("project_id").value = "1"
      dateTimeField("due_date").value = "07/13/2016"
      submit()
      currentUrl must be ("http://localhost:" + port + "/tasks/reported_by_me")
      pageSource must include ("Zadanie-1")
      click on linkText("Podgląd")
      currentUrl must include ("/tasks/show")
      pageTitle must be("test-1")
      pageSource must include ("Opis:")
      pageSource must include ("Termin wykonania:")
      pageSource must include ("Aktualny status:")
      pageSource must include ("Wykonawca:")
      pageSource must include ("Zgłaszający:")
    }
  }

}
