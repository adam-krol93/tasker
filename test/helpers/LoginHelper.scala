package helpers

import org.openqa.selenium.WebDriver
import org.scalatest.selenium.WebBrowser
import org.scalatestplus.play.OneServerPerTest
import play.api.test.Helpers

/**
  * Created by Adam Król, Atos <adam.krol@atos.net>
  */
object LoginHelper {

  def login()(implicit driver:WebDriver) : Unit ={
    WebBrowser.go.to("http://localhost:" + Helpers.testServerPort)
    WebBrowser.textField("username").value = "admin"
    WebBrowser.id("password").webElement.sendKeys("admin")
    WebBrowser.submit
  }
}
