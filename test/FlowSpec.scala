import helpers.LoginHelper
import org.scalatest.BeforeAndAfterEach
import org.scalatestplus.play.{HtmlUnitFactory, OneBrowserPerTest, OneServerPerTest, PlaySpec}

/**
  * Created by dawid.wojna on 14.07.2016.
  */
class FlowSpec extends PlaySpec with OneServerPerTest with OneBrowserPerTest with HtmlUnitFactory with BeforeAndAfterEach {
  "Application" should {
    "show list of flows" in {
      LoginHelper.login()
      go to ("http://localhost:" + port + "/flow/list")
      pageTitle must be("Lista kroków")
      pageSource must include("Kroki")
      pageSource must include("Nowe")
      pageSource must include("W trakcie")
      pageSource must include("Zrobione")
      pageSource must include("Edytuj")
      pageSource must include("Edytuj przejścia")
    }
  }

  "Admin" should {
    "be able to create new step" in {
      LoginHelper.login()
      go to ("http://localhost:" + port + "/flow/add")
      pageTitle must be("Dodawanie kroku")
      pageSource must include("Krok")
      pageSource must include("Nazwa kroku")
      textField("name").value = "testStep"
      submit()
      pageTitle must be("Lista kroków")
      pageSource must include("testStep")
    }

    "be able to configure flow for existing step" in {
      LoginHelper.login()
      go to ("http://localhost:" + port + "/flow/1/outgoing")
      pageTitle must be("Konfigurowanie przejść")
      pageSource must include("Konfiguracja przejść dla kroku: Nowe")
      pageSource must include("W trakcie")
      pageSource must include("Zrobione")
      pageSource must include("Zapisz")
    }

    "be able to edit step" in {
      LoginHelper.login()
      go to ("http://localhost:" + port + "/flow/edit/1")
      pageTitle must be("Edycja kroku")
      pageSource must include("Krok")
      pageSource must include("Nazwa kroku")
      pageSource must include("Nowe")
      pageSource must include("Zapisz")
      textField("name").value = "Nowy po zmianie"
      submit()
      pageTitle must be("Lista kroków")
      pageSource must include("Nowy po zmianie")
    }
  }
}
