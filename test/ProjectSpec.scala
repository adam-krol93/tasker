import helpers.LoginHelper
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, Outcome}
import org.scalatestplus.play.{HtmlUnitFactory, OneBrowserPerTest, OneServerPerTest, PlaySpec}

/**
  * Created by Adam Król, Atos <adam.krol@atos.net>
  */
class ProjectSpec extends PlaySpec with OneServerPerTest with OneBrowserPerTest with HtmlUnitFactory with BeforeAndAfterEach   {



  "Application" should {
    "show list of projects" in {
      LoginHelper.login()
      go to ("http://localhost:" + port+"/project/list")
      pageTitle must be("Lista projektów")
    }

    "should allow user to create new project" in {
      LoginHelper.login()
      go to ("http://localhost:" + port + "/project/list")
      pageTitle must be("Lista projektów")
      click on linkText("Dodaj projekt")
      pageTitle must be("Dodawanie projektu")
      pageSource must include("Nowy projekt")
    }

    "should not allow add another test project" in {
      LoginHelper.login()
      go to ("http://localhost:" + port+"/project/add")
      textField("key").value = "test"
      textField("name").value = "aaaa"
      textField("description").value = "aaaaa"
      singleSel("manager_id").value = "1"
      submit()
      pageSource must include("Taki klucz już istenieje")
      pageSource must include("error")
    }

    "should allow create another project" in {
      LoginHelper.login()
      go to ("http://localhost:" + port+"/project/add")
      textField("key").value = "test2"
      textField("name").value = "kolejny projekt"
      textField("description").value = "aaaaa"
      singleSel("manager_id").value = "1"
      submit()
      currentUrl contains "project/list"
      pageSource contains "kolejny projekt"
    }
  }
}
