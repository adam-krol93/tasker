import helpers.LoginHelper
import org.scalatestplus.play._
import play.api.test._
import play.api.test.Helpers._


class IntegrationSpec extends PlaySpec with OneServerPerTest with OneBrowserPerTest with HtmlUnitFactory {

  "Application" should {

    "work from within a browser" in {

      go to ("http://localhost:" + port)

      pageTitle must be ("Tasker - logowanie")
      pageSource must include ("Nazwa użytkownika")
    }
  }

  "User" should {
    "be able to log in" in {

      go to ("http://localhost:" + port)
      textField("username").value = "admin"
      id("password").webElement.sendKeys("admin")
      submit
      eventually {
        pageTitle must be("Tasker")
        pageSource must include ("Dashboard")
      }
    }

    "be able to log out" in {
      LoginHelper.login()
      go to ("http://localhost:" + port+ "/logout")
      pageTitle must be("Tasker - logowanie")
      pageSource must include ("Zaloguj się")
      pageSource must include ("Nazwa użytkownika")
      pageSource must include ("Hasło")
    }
  }
}
