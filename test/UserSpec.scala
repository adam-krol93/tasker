import helpers.LoginHelper
import org.scalatest.BeforeAndAfterEach
import org.scalatestplus.play.{HtmlUnitFactory, OneBrowserPerTest, OneServerPerTest, PlaySpec}

/**
  * Created by dawid.wojna on 13.07.2016.
  */
class UserSpec extends PlaySpec with OneServerPerTest with OneBrowserPerTest with HtmlUnitFactory with BeforeAndAfterEach {
  "Application" should {
    "show list of users when i'm admin" in {
      LoginHelper.login()
      go to ("http://localhost:" + port + "/users/list")
      pageTitle must be("Lista użytkowników")
      pageSource must include("Użytkownicy")
      pageSource must include("admin")
      pageSource must include("Edytuj")
      pageSource must include("Usuń")
      pageSource must include("Dodaj użytkownika")
    }
  }

  "Admin" should {
    "be able to add new user" in {
      LoginHelper.login()
      go to ("http://localhost:" + port + "/users/list/add")
      pageTitle must be("Dodawanie użytkownika")
      pageSource must include("Użytkownik")
      pageSource must include("Nazwa użytkownika")
      pageSource must include("Hasło")
      pageSource must include("admin")
      pageSource must include("member")
      textField("username").value = "Test1"
      click on "password"
      enter("Test1Pass!")
      submit()
      go to ("http://localhost:" + port + "/users/list")
      pageTitle must be("Lista użytkowników")
      pageSource must include("Jest 2 użytkowników")
    }

    "be able to edit existing user" in {
      LoginHelper.login()
      go to ("http://localhost:" + port + "/users/list/1/edit")
      pageTitle must be("Edytowanie użytkownika")
      pageSource must include("Użytkownik")
      pageSource must include("Nazwa użytkownika")
      pageSource must include("Hasło")
      pageSource must include("admin")
      pageSource must include("member")
      pageSource must include("Zapisz")
    }

    "be able to remove existing user" in {
      LoginHelper.login()
      go to ("http://localhost:" + port + "/users/list/add")
      textField("username").value = "Test1"
      click on "password"
      enter("Test1Pass")
      submit()
      pageTitle must be("Lista użytkowników")
      pageSource must include("Jest 2 użytkowników")
      go to ("http://localhost:" + port + "/users/list/2/delete")
      pageTitle must be("Lista użytkowników")
      pageSource must include("Jest 1 użytkowników")
    }
  }
}
