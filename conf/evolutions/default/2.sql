# Test schema

# --- !Ups
ALTER TABLE `users` ADD COLUMN `roles` VARCHAR(255);

# --- !Downs
ALTER TABLE `users` DROP COLUMN `roles`;