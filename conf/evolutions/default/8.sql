# Test schema

# --- !Ups

insert into flow_step values(1,'Nowe'),(2,'W trakcie'),(3,'Zrobione');
insert into flow_step_to_flow_step values(1,2),(1,3),(2,3),(3,1);

ALTER TABLE project ADD COLUMN first_step_id bigint;
ALTER TABLE project ADD COLUMN last_step_id bigint;

update project set first_step_id=1,last_step_id=3;


# --- !Downs
ALTER TABLE project DROP first_step_id;
ALTER TABLE project DROP last_step_id;
