# Test schema

# --- !Ups
create table `users` (
  `id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `username` VARCHAR(55) NOT NULL,
  `password` VARCHAR(255) NOT NULL
);

# --- !Downs
drop table if exists `users`;