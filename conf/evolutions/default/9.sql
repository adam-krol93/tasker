# Test schema

# --- !Ups
ALTER TABLE `task` ADD COLUMN `due_date` DATETIME;
# --- !Downs
ALTER TABLE `task` DROP COLUMN `due_date`;
