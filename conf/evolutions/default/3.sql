# Test schema

# --- !Ups
create table project (
  id                       BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  project_key                      varchar(15),
  name                      varchar(100),
  description               varchar(255),
  manager_id                bigint
)
;


create table project_user (
  project_id                     bigint not null,
  username                 		 varchar(255) not null,
  constraint pk_project_user primary key (project_id, username));

# --- !Downs
drop table if exists `project`;
drop table if exists `project_user`;
