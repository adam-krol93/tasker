# Test schema

# --- !Ups

create table task (
  id          BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  task_key    VARCHAR(255),
  summary     VARCHAR(255),
  description VARCHAR(255),
  project_id  BIGINT,
  assignee_id BIGINT NULL,
  reporter_id BIGINT NULL,
  state_id    BIGINT NULL,
  FOREIGN KEY (assignee_id) REFERENCES users (id),
  FOREIGN KEY (reporter_id) REFERENCES users (id),
  FOREIGN KEY (state_id) REFERENCES flow_step (id),
  FOREIGN KEY (project_id) REFERENCES project (id)
);

# --- !Downs
drop table if exists `task`;
