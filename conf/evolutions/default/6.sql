# Test schema

# --- !Ups
ALTER TABLE `project` ADD COLUMN `isDeleted` BOOLEAN;
INSERT INTO project VALUES (1, 'test', 'test', 'test', 1, false);
INSERT INTO project VALUES (2, 'test2', 'test2', 'test2', 1, true);
# --- !Downs
DELETE FROM project WHERE id in (1,2);
ALTER TABLE `project` DROP COLUMN `isDeleted`;
