# --- !Ups
INSERT INTO users VALUES (1, 'admin', '$2a$10$tfxNOJyEWY1zjkULQQp6EeKZHrnce5Y2xowx2upcG6wdrTtREdu/a', 'admin');


# --- !Downs
DELETE FROM users WHERE id=1;