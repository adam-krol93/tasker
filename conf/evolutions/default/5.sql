# Test schema

# --- !Ups
create table `flow_step` (
  `id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `flow_name` VARCHAR(55) NOT NULL
);

CREATE TABLE `flow_step_to_flow_step` (
  `flow_id` BIGINT NOT NULL,
  `outgoing_flow_id` BIGINT NOT NULL,
  PRIMARY KEY (flow_id,outgoing_flow_id),
  FOREIGN KEY (flow_id) REFERENCES flow_step(id),
  FOREIGN KEY (outgoing_flow_id) REFERENCES flow_step(id)
);

# --- !Downs
drop table if exists `flow_step`;
drop table if exists `flow_step_to_flow_step`;