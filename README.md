# Tasker 

[![build status](https://gitlab.com/adam-krol93/tasker/badges/master/build.svg)](https://gitlab.com/adam-krol93/tasker/commits/master)

Aplikacja do zarządzania projektami napisana na potrzeby projektu "Programowanie w języku Scala".

### Autorzy:
- Adam Król
- Piotr Jurewicz
- Dawid Wojna
- Marcin Kabza

### Wymagania:
- Java 8

### Instalacja
1. Zainstaluj Jave
1. Zainstaluj Scalę
1. Zainstaluj SBT
1. Uruchom `sbt run`
1. Aplikacja jest dostępna domyślnie pod adresem localhost:9000
1. Po uruchomieniu należy wykonać ewolucję przycisk "Apply script now" - spowodowane wbudowaną bazą H2
1. Dane potrzebne do zalogowania login: admin, hasło: admin

